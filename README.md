# Cuttle

Starting as a CLI application, with a view to make it playable online.

Primarily an exercise in rapid development with Python.

## Rules

First to 21 or above

Dealer: 6 cards
Opponent: 5 cards (goes first)
Turn: either take or place a card

Cuttling: Value, Spade > Heart > Diamond > Club

Points: A, 2, 3, 4, 5, 6, 7, 8, 9, 10

One-off effect cards:
    A: Scrap all points cards
    2: a) Scrap single permanent effect card
        b) Deny one-off as it happens
    3: Take any card from discard pile
    4: Opponent discard two choice hand cards
    5: Take two from draw pile
    6: Scrap all permanent effect cards
    7: Draw card and play it immediately
    9: Permanent effect card back in controllers hand

Permanent effect cards:
    8: Glasses (see the other players hand)
    J: Take control of a points card
    Q: Protects against 2a, 9 & J
    K: Reduce your target points
         0) 21 points
         1) 14 points
         2) 10 points
         3) 7 points
         4) 5 points