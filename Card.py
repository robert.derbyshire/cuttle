class Card:
    faces = ['A', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K']
    suits = ['S', 'H', 'D', 'C']

    def __init__(self, face, suit):
        self.suit = suit
        self.face = face

    def is_points_card(self):
        return self.face in ['A', '2', '3', '4', '5', '6', '7', '8', '9', 'T']

    def get_value(self):
        return { 'A': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, 'T': 10, 'J': 0, 'Q': 0, 'K': 0 }.get(self.face, 0)

    def __str__(self):
        return f"[{self.face}{self.suit} ({self.get_value()})]"

    def __eq__(self, other):
        if isinstance(other, Card):
            return self.suit == other.suit and self.face == other.face
        return False

    def __gt__(self, other):
        if isinstance(other, Card):
            return Card.faces.index(self.face) > Card.faces.index(other.face) or (Card.faces.index(self.face) == Card.faces.index(other.face) and Card.suits.index(self.suit) > Card.suits.index(other.suit))
        raise TypeError(f"Can't compare with items that aren't of type {type(self)}")

    def __lt__(self, other):
        if isinstance(other, Card):
            return Card.faces.index(self.face) < Card.faces.index(other.face) or (Card.faces.index(self.face) == Card.faces.index(other.face) and Card.suits.index(self.suit) < Card.suits.index(other.suit))
        raise TypeError(f"Can't compare with items that aren't of type {type(self)}")
