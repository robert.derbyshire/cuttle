import random

from Player import Player

class Cuttle:
    def __init__(self, players, deck):
        self.deck = deck
        self.discard_pile = []
        self.players = [Player(player) for player in players]
        random.shuffle(self.players)
        self.current_player = 1

    def deal_round(self):
        for x in range((len(self.players) * 6) - 1):
            self.players[(x + 2) % len(self.players)].pickup_card(self.deck)

    def print_deck(self):
        for player in self.players:
            print(player)
            player.print_hand()

    def print_game(self):
        current_player = self.players[self.current_player]

        for player in self.players:
            hand = ", ".join([str(card) for card in player.hand])
            points_cards = ", ".join([str(card) for card in player.points_cards])
            action_cards = ", ".join([str(card) for card in player.permanent_effect_cards])
            print(f"{player.name}{'*' if current_player is player else ''}:")
            print(f"  Hand:\t{hand}")
            print(f"  Points:\t{points_cards}")
            print(f"  Permanent:\t{action_cards}")

        print('-' * 40)

    def is_current_player(self, player):
        return player is self.players[self.current_player]

    def next_turn(self):
        self.current_player = (self.current_player + 1) % len(self.players)
        self.print_game()

    def draw_card(self, player):
        if ():
            player.pickup_card(self.deck)
            self.next_turn()
        else:
            print("It's not your turn")

    def play_points_card(self, player, card):
        if (player is self.players[self.current_player]):
            if (card in player.hand and card.is_points_card()):
                player.points_cards.append(card)
                player.hand.remove(card)
                self.next_turn()

    def scuttle(self, player, card, playerToScuttle, cardToScuttle):
        if (player is self.players[self.current_player] and playerToScuttle is not player):
            if (card.is_points_card() and cardToScuttle.is_points_card() and card > cardToScuttle):
                if (cardToScuttle in playerToScuttle.points_cards):
                    playerToScuttle.points_cards.remove(cardToScuttle)
                    self.discard_pile.append(cardToScuttle)
                    self.discard_pile(card)
                    self.next_turn()

    def play_ace(self, player, card):
        if self.is_current_player(player) and card in player.hand and card.face == 'A':
            cards_to_discard = [c for c in p.points_cards for p in self.players]
            cards_to_discard.shuffle()
            self.discard_pile.extend(cards_to_discard)
            self.discard_pile.add(card)
            self.next_turn()

    def play_two(self, player, card, targetPlayer, targetCard):
        if self.is_current_player(player) and card in player.hand and card.face == '2':
            if targetPlayer is not player and targetPlayer in self.players:
                if targetCard in targetPlayer.action_cards:
                    if targetCard.face == 'Q' or not any(card.face == 'Q' for card in targetPlayer.action_cards):
                        targetPlayer.action_cards.remove(targetCard)
                        self.discard_pile.append(targetCard)
                        self.discard_pile.append(card)

    def play_three(self, player, card, chosenCard):
        if self.is_current_player(player) and card in player.hand and card.face == '3':
            if chosenCard in self.discard_pile:
                self.discard_pile.remove(chosenCard)
                player.hand.append(chosenCard)
                self.discard_pile.append(card)

    def play_four(self, player, card):
        if self.is_current_player(player) and card in player.hand and card.face == '4':
            self.discard_pile.append(card)

    def play_five(self, player, card):
        if self.is_current_player(player) and card in player.hand and card.face == '5':
            self.discard_pile.append(card)
            player.pickup_card(self.deck)
            player.pickup_card(self.deck)
            self.next_turn()

    def play_six(self, player, card):
        if self.is_current_player(player) and card in player.hand and card.face == '6':
            cards_to_discard = [c for c in p.permanent_effect_cards for p in self.players]
            cards_to_discard.shuffle()
            self.discard_pile.extend(cards_to_discard)
            self.discard_pile.add(card)
            self.next_turn()

    def play_seven(self, player, card):
        if self.is_current_player(player) and card in player.hand and card.face == '7':
            None
        
    def play_eight(self, player, card):
        if self.is_current_player(player) and card in player.hand and card.face == '8':
            None
            
    def play_nine(self, player, card, targetPlayer, targetCard):
        if self.is_current_player(player) and card in player.hand and card.face == '9':
            None
    

    


    def play_permanent_effect(self, player, card):
        

    def play_one_off_effect(self, player, card):
        None