class Player:
    def __init__(self, name):
        self.name = name
        self.hand = []
        self.points_cards = []
        self.permanent_effect_cards = []

    def pickup_card(self, deck):
        self.hand.append(deck.pop())

    def print_hand(self):
        [print(card) for card in self.hand]

    def play_points_cards(self, card):
        if (card in self.hand):
            self.hand.remove(card)
            self.points_cards.append(card)

    def play_one_off_effect(self, card):
        if (card in self.hand):
            None

    def __str__(self):
        return f"Player({self.name})"