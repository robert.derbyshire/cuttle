#!/usr/bin/env python3

from random import Random
from Cuttle import Cuttle
from Card import Card

def create_new_shuffled_deck (seed=None):
    cards = [Card(face, suit) for suit in Card.suits for face in Card.faces]
    Random(seed).shuffle(cards)
    return cards

g = Cuttle(['Rob', 'Chris'], create_new_shuffled_deck(0))
g.deal_round()
g.print_game()
g.play_points_card(g.players[1], g.players[1].hand[0])
g.draw_card(g.players[1])


queen_clubs = Card('Q', 'C')
queen_spades = Card('Q', 'S')
seven_hearts = Card('7', 'H')

assert (queen_spades > queen_spades) == False
assert (queen_spades > queen_clubs) == False
assert (queen_spades > seven_hearts) == True
assert (queen_spades < queen_spades) == False
assert (queen_spades < queen_clubs) == True
assert (queen_spades < seven_hearts) == False
assert (queen_spades == queen_spades) == True
assert (queen_spades == queen_clubs) == False
assert (queen_spades == seven_hearts) == False

assert (queen_clubs > queen_spades) == True
assert (queen_clubs > queen_clubs) == False
assert (queen_clubs > seven_hearts) == True
assert (queen_clubs < queen_spades) == False
assert (queen_clubs < queen_clubs) == False
assert (queen_clubs < seven_hearts) == False
assert (queen_clubs == queen_spades) == False
assert (queen_clubs == queen_clubs) == True
assert (queen_clubs == seven_hearts) == False

assert (seven_hearts > queen_spades) == False
assert (seven_hearts > queen_clubs) == False
assert (seven_hearts > seven_hearts) == False
assert (seven_hearts < queen_spades) == True
assert (seven_hearts < queen_clubs) == True
assert (seven_hearts < seven_hearts) == False
assert (seven_hearts == queen_spades) == False
assert (seven_hearts == queen_clubs) == False
assert (seven_hearts == seven_hearts) == True

